package com.rubenlg94.subscriptionservice.repositories;

import com.rubenlg94.subscriptionservice.entities.Campaign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CampaignRepository extends JpaRepository<Campaign, Long> {

    Campaign findByIdEquals(Long id);

}
