package com.rubenlg94.subscriptionservice.services;

import com.rubenlg94.subscriptionservice.exceptions.SubscriptionException;
import com.rubenlg94.subscriptionservice.models.SubscriptionModel;

import java.util.List;

public interface SubscriptionService {

    List<SubscriptionModel> findAll();

    Long createSubscription(SubscriptionModel subscriptionModel) throws SubscriptionException;

    Long cancelSubscription(Long id);

    SubscriptionModel findById(Long id);

}
