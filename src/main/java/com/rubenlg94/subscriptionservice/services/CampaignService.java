package com.rubenlg94.subscriptionservice.services;

import com.rubenlg94.subscriptionservice.models.CampaignModel;

public interface CampaignService {

    public CampaignModel findById(Long id);

}
