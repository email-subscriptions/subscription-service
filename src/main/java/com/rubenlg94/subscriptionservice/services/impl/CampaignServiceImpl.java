package com.rubenlg94.subscriptionservice.services.impl;

import com.rubenlg94.subscriptionservice.models.CampaignModel;
import com.rubenlg94.subscriptionservice.repositories.CampaignRepository;
import com.rubenlg94.subscriptionservice.services.CampaignService;
import org.springframework.stereotype.Service;

@Service
public class CampaignServiceImpl implements CampaignService {

    private final CampaignRepository campaignRepository;

    public CampaignServiceImpl(CampaignRepository campaignRepository) {
        this.campaignRepository = campaignRepository;
    }

    @Override
    public CampaignModel findById(Long id) {
        return CampaignModel.from(campaignRepository.findByIdEquals(id));
    }
}
