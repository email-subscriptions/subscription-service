package com.rubenlg94.subscriptionservice.services.impl;

import com.rubenlg94.subscriptionservice.entities.Campaign;
import com.rubenlg94.subscriptionservice.entities.Subscription;
import com.rubenlg94.subscriptionservice.exceptions.SubscriptionException;
import com.rubenlg94.subscriptionservice.models.SubscriptionModel;
import com.rubenlg94.subscriptionservice.repositories.CampaignRepository;
import com.rubenlg94.subscriptionservice.repositories.SubscriptionRepository;
import com.rubenlg94.subscriptionservice.services.SubscriptionService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

    private final SubscriptionRepository subscriptionRepository;
    private final CampaignRepository campaignRepository;

    public SubscriptionServiceImpl(SubscriptionRepository subscriptionRepository, CampaignRepository campaignRepository) {
        this.subscriptionRepository = subscriptionRepository;
        this.campaignRepository = campaignRepository;
    }

    @Override
    public List<SubscriptionModel> findAll() {
        return subscriptionRepository.findAll().stream().map(SubscriptionModel::from).collect(Collectors.toList());
    }

    @Override
    public Long createSubscription(SubscriptionModel subscriptionModel) throws SubscriptionException {
        Subscription subscription = new Subscription();
        subscription.setEmail(subscriptionModel.getEmail());
        subscription.setFirstName(subscriptionModel.getFirstName());
        subscription.setGender(subscriptionModel.getGender());
        subscription.setConsent(subscriptionModel.getConsent());
        subscription.setCreatedOn(LocalDateTime.now());
        subscription.setEnabled(subscriptionModel.getConsent());
        Campaign campaign = campaignRepository.findByIdEquals(subscriptionModel.getCampaignId());
        if(campaign == null) {
            throw new SubscriptionException("Campaign doesn't exist");
        }
        Subscription alreadyExist = subscriptionRepository.findSubscriptionByEmailEqualsAndCampaignEquals(subscriptionModel.getEmail(), campaign);
        if(alreadyExist != null) {
            throw new SubscriptionException("Subscription already exist");
        }
        subscription.setCampaign(campaign);
        return subscriptionRepository.save(subscription).getId();
    }

    @Override
    public Long cancelSubscription(Long id) {
        return null;
    }

    @Override
    public SubscriptionModel findById(Long id) {
        return SubscriptionModel.from(subscriptionRepository.findSubscriptionByIdEquals(id));    }

}
