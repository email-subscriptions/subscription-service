package com.rubenlg94.subscriptionservice.models;

import com.rubenlg94.subscriptionservice.entities.Campaign;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class CampaignModel {

    private Long id;
    private String name;
    private LocalDateTime createdOn;
    private LocalDateTime updatedOn;
    private List<SubscriptionModel> subscriptionModels;

    public static CampaignModel from(Campaign campaign){
        CampaignModel campaignModel = new CampaignModel();
        campaignModel.setId(campaign.getId());
        campaignModel.setName(campaign.getName());
        campaignModel.setCreatedOn(campaign.getCreatedOn());
        campaignModel.setUpdatedOn(campaign.getUpdatedOn());
        campaignModel.setSubscriptionModels(campaign.getSubscriptions().stream().map(SubscriptionModel::from).collect(Collectors.toList()));
        return campaignModel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public LocalDateTime getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDateTime updatedOn) {
        this.updatedOn = updatedOn;
    }

    public List<SubscriptionModel> getSubscriptionModels() {
        return subscriptionModels;
    }

    public void setSubscriptionModels(List<SubscriptionModel> subscriptionModels) {
        this.subscriptionModels = subscriptionModels;
    }
}
