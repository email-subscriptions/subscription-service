package com.rubenlg94.subscriptionservice.components;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthenticationApiKeyFilter extends OncePerRequestFilter {

    @Value("${api-key.header}")
    private String header;

    @Value("${api-key.secret}")
    private String secret;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        String apikey = request.getHeader(header);
        if (apikey == null || !apikey.equals(secret)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "API-KEY Incorrecta");
        }
        filterChain.doFilter(request, response);
    }

}
