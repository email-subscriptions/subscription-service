package com.rubenlg94.subscriptionservice.entities;

import com.rubenlg94.subscriptionservice.constants.DatabaseValues;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = DatabaseValues.Campaign.TABLE_NAME)
public class Campaign implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DatabaseValues.Campaign.ID)
    private Long id;

    @Column(name = DatabaseValues.Campaign.NAME)
    private String name;

    @Column(name = DatabaseValues.Campaign.CREATED_ON)
    private LocalDateTime createdOn;

    @Column(name = DatabaseValues.Campaign.UPDATED_ON)
    private LocalDateTime updatedOn;

    @OneToMany(mappedBy = "campaign")
    private List<Subscription> subscriptions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public LocalDateTime getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDateTime updatedOn) {
        this.updatedOn = updatedOn;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }
}
