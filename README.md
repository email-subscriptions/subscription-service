# subscription-service

Microservice that implements subscription logic, including persistence of subscription data in a database and email notification to confirm process is completed.